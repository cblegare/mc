.. _usage:

=====
Usage
=====

|fjordlynn| is a Minecraft Server tool box.

.. click:: fjordlynn.cli:main
    :prog: fjordlynnctl
    :show-nested:
