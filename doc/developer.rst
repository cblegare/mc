.. _developer:

#######################
Developer's Cheat Sheet
#######################

A developer guide and memory dump.  We will use Kubernetes (K8s)
to host the project.

Kubernetes is an open-source system for automating deployment,
scaling, and management of containerized applications.

Working with K8s requires a lot of tools.  We'll start with
getting them.


Getting the tools
=================

Docker
------

Docker provides a way to run applications securely isolated in a
container, packaged with all its dependencies and libraries.

`Upstream guide to installing Docker`_

.. _Upstream guide to installing Docker: https://docs.docker.com/install/linux/docker-ce/ubuntu/


Python
------

In order to develop in |fjordlynn|, a working Python development environment
must be setup.  Install the latest Python interpreter, including `pip`.

Then, create an isolated virtual environment including all libraries and
tools:

.. code-block:: shell

    python -m venv env \
        source env/bin/activate \
        pip install -U pip -e ".[all]"


Manual test of docker images
=============================

Testing |fjordlynn|
-------------------

Build it with the docker daemon running in your cluster.

.. code-block:: shell

    docker build -t cblegare/fjordlynn:local .

Run the |fjordlynn| cli from the cluster:

.. code-block:: shell

    docker run cblegare/fjordlynn:local fjordlynn --help


Running Minecraft
-----------------

Run a simple Minecraft server using a local path for the server data.

.. code-block:: shell

    export PLAYERNAME=cblegare
    export WORLDNAME=fjordlynn

    mkdir -p srv/data/worlds/$WORLDNAME
    mkdir -p srv/storage/worlds/$WORLDNAME

    docker run \
        -e EULA=TRUE \
        -e LEVEL=$WORLDNAME \
        -e TYPE=PAPER -e VERSION=1.13.2 \
        -e DIFFICULTY=hard \
        -e OPS=$PLAYERNAME \
        -p 25565:25565 -p 8123:8123 \
        -v $(pwd)/srv/storage/worlds/$WORLDNAME:/data \
        --name mc itzg/minecraft-server;
