.. _installation:

#########################
Deploying to Google Cloud
#########################

**THIS SECTION IS ABSOLUTELY INCOMPLETE**

First, make sure you have a project in which billing is linked and APIs
are enabled

Given:

``PROJECT_ID``

    Your Google Cloud Project ID.  This might have been auto-generated
    by Google.

``REGION``

    Select a region for your infrastructure from any `available regions`_.

``SERVER_NAME``

    A name to identify your server.  |fjordlynn| could be used to host
    several servers on the same infrastructure.

.. _available regions: https://cloud.google.com/compute/docs/regions-zones/

The following command snippets will use these values as environment
variables.  In the shell you run the commands, define them like so:

.. code-block:: shell

    export PROJECT_ID=my-project-id

.. hint::

    You will need the ``gcloud`` command line tool.  Here is a
    *command* that install it in your VirtualEnv.

    .. code-block:: shell

        mkdir -p "env/opt" && \
        curl https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-242.0.0-linux-x86_64.tar.gz | tar -zxv -C "env/opt" && \
        CLOUDSDK_PYTHON="env/bin/python" "env/opt/google-cloud-sdk/install.sh" \
            --usage-reporting false \
            --rc-path "env/bin/activate" \
            --additional-components \
                docker-credential-gcr \
                beta \
                alpha \
                kubectl \
            --quiet && \
        source "env/bin/activate"


Setup Storage
=============

We use Google Cloud Storage for persistence.  Let's create a bucket for our
project in the same region we deployed the cluster.  Since we will read and
write often from it, we use ``regional`` storage.

.. code-block:: shell

    gsutil mb -p $PROJECT_ID -c regional -l $REGION gs://fjordlynn/

Also, we need a persona for managing persistence on our behalf.  Introducing
Urist McArchivist!  Let's create a service account for managing the storage
from within the service.

.. code-block:: shell

    export URIST_SANAME="fjordlynn-archivist"

    gcloud iam service-accounts create \
        $URIST_SANAME \
        --display-name "Urist McArchivist"

    export URIST_EMAIL="$URIST_SANAME@$PROJECT_ID.iam.gserviceaccount.com"

We created a Service Account named ``fjordlynn-archivist``.  A service
account is usually referenced by its email address, which is stored in
``$URIST_EMAIL``.  Urist needs permissions to create, read update and
delete files and folders of this storage only.

.. code-block:: shell

    gsutil iam ch \
        serviceAccount:$URIST_EMAIL:objectAdmin \
        gs://fjordlynn

Our storage implementation will need to impersonate Urist in order
to save de world.  Create a JSON key for Urist and authenticate with it from the service which is executing the server code.

.. code-block:: shell

    gcloud iam service-accounts keys create urist-key.json \
    --iam-account=$URIST_EMAIL


.. code-block:: shell

    gcloud auth activate-service-account --key-file=urist-key.json



Understanding |fjordlynn| storage
---------------------------------

|fjordlynn| needs the storage to be arranged like so:

.. code-block:: none

    gs://fjordlynn/  # This is the root: your bucket.
    └── worlds/
        └── $SERVER_NAME/  # This
            ├── active/    # The currently in used in production world.
            │   ├── eula.txt
            │   └── ...    # All the server files.
            ├── latest/    # This is the latest working copy of your world
            ├── 2019-04-29T02:17:35+00:00/  # One or many backups by timestamp
            └── ...

Here are a few definitions

**active**

    This copy of your world is currently in use in production.  Urist
    should be the only one writing to this folder. In case of a crash,
    this can get corrupt.

**latest**

    This is the latest *backup* copy. This is the copy the world starts
    from (the server first restore this backup into its working copy).
    Also, this copy can be used by external read-only services like
    a map renderer.

backups by timestamps

    There is no support yet for automated backup.  If you want some,
    I strongly suggest to name the folder using the ISO 8601 format
    including timezone information like in the example.

For a better understanding, here is the script Urist runs every few
minutes

.. code-block:: shell

    # (stop Minecraft from saving to the filesystem)
    gsutils -m rsync -dpr \
        /datadir \
        gs://fjordlynn/worlds/$SERVER_NAME/active
    # (resume Minecraft saving to filesystem)
    gsutils -m rsync -dpr \
        gs://fjordlynn/worlds/$SERVER_NAME/active
        gs://fjordlynn/worlds/$SERVER_NAME/latest


Initialize a World
==================

To get started, you first need to generate a world.  |fjordlynn| uses
the `itzg/minecraft-server`_ Docker image by default.  You can use it to
generate a base world.  Juste make sure the parameters you pass it are the
same you use for :ref:`configuration`, especially the world generation ones.

.. code-block:: shell

    export WORLDNAME=myworld

    docker run \
        -e EULA=TRUE \
        -e LEVEL=$WORLDNAME \
        -e TYPE=PAPER -e VERSION=1.15.2 \
        -e DIFFICULTY=hard \
        -e OPS=cblegare,Octorax \
        -p 25565:25565 \
        -v $(pwd)/srv/storage/worlds/$WORLDNAME:/data \
        --name mc itzg/minecraft-server

.. _itzg/minecraft-server: https://hub.docker.com/r/itzg/minecraft-server/

Before starting the server, the World needs to already be stored.  Take
a few minutes to generate your world (provision a seed, pre-generate some
chunks, etc.).  This includes server configuration.  Have a look at the
|fjordlynn| CLI that might provide some tooling.

By default, after running `docker run`, your world will appear in /src/storage/worlds/myworld

When your world is ready (say in the ``myworld`` folder), copy it to the
cloud as the ``latest`` copy:

.. code-block:: shell

    cd srv/storage/worlds
    gsutil -m cp -r myworld gs://fjordlynn/worlds/$SERVER_NAME/latest


Entering your World
===================

You will need the minecraft client to connect to your freshly spawned server.

.. code-block:: shell

    wget https://launcher.mojang.com/download/Minecraft.deb
    sudo apt-get install Minecraft.deb
