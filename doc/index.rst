###########
|fjordlynn|
###########

A toy project for managing and extending a Minecraft_ server.

.. only:: html

    |home_badge| |doc_badge| |lic_badge|

    .. |home_badge| image:: https://img.shields.io/badge/gitlab-fjordlynn%fjordlynn-orange.svg
        :target: https://gitlab.com/cblegare/mc
        :alt: Sources

    .. |doc_badge| image:: https://img.shields.io/badge/docs-fjordlynn-blue.svg
        :target: https://cblegare.gitlab.io/mc
        :alt: Home Page

    .. |lic_badge| image:: https://img.shields.io/badge/License-LGPL%20v3-blue.svg
        :target: http://www.gnu.org/licenses/lgpl-3.0
        :alt: GNU Lesser General Public License v3

    Download the |pdflink|.


* `Source code on Gitlab`_
* `Latest documentation`_
* `Issue tracker on Gitlab`_
* `Todo list on Gitlab`_

|fjordlynn| is free software and licensed under the GNU Lesser General Public
License v3.

.. _Minecraft: https://www.minecraft.net/en-us/
.. _Source code on Gitlab: https://gitlab.com/cblegare/mc
.. _Latest documentation: https://cblegare.gitlab.io/mc/
.. _Issue tracker on Gitlab: https://gitlab.com/cblegare/mc/issues
.. _Todo list on Gitlab: https://gitlab.com/cblegare/mc/boards/414384?=


.. toctree::
    :maxdepth: 2

    usage
    installation
    configuration
    changelog
    authors
    license


======================
Technical Informations
======================

.. only:: html

    |build_badge| |cov_badge|

    .. |build_badge| image:: https://gitlab.com/cblegare/mc/badges/master/pipeline.svg
        :target: https://gitlab.com/cblegare/mc/pipelines
        :alt: Build Status

    .. |cov_badge| image:: https://gitlab.com/cblegare/mc/badges/master/coverage.svg
        :target: _static/embedded/coverage/index.html
        :alt: Coverage Report

Here lies guides and references for contributors as well as some general
references.

.. toctree::
    :maxdepth: 2

    developer
    arch/index
    contributing
    modules


.. only:: html

    ==================
    Tables and indices
    ==================

    Quick access for not so much stuff.

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
