A toy project for managing and extending a Minecraft_ server.

|build_badge| |cov_badge| |lic_badge|

* `Source code on Gitlab`_
* `Latest documentation`_
* `Issue tracker on Gitlab`_
* `Todo list on Gitlab`_

`fjordlynn` is free software and licensed under the GNU Lesser General Public
License v3.

.. _Minecraft: https://minecraft.net
.. _Source code on Gitlab: https://gitlab.com/cblegare/mc
.. _Latest documentation: https://cblegare.gitlab.io/mc/
.. _Issue tracker on Gitlab: https://gitlab.com/cblegare/mc/issues
.. _Todo list on Gitlab: https://gitlab.com/cblegare/mc/boards/414384?=

.. |build_badge| image:: https://gitlab.com/cblegare/mc/badges/master/pipeline.svg
    :target: https://gitlab.com/cblegare/mc/pipelines
    :alt: Build Status

.. |cov_badge| image:: https://gitlab.com/cblegare/mc/badges/master/coverage.svg
    :target: https://cblegare.gitlab.io/mc/_static/embedded/coverage/index.html
    :alt: Coverage Report

.. |lic_badge| image:: https://img.shields.io/badge/License-LGPL%20v3-blue.svg
    :target: http://www.gnu.org/licenses/lgpl-3.0
    :alt: GNU Lesser General Public License v3
