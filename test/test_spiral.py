from functools import reduce
from itertools import cycle

from fjordlynn.spiral import walk_spiral


def test_10_first():
    """
        6 7 8 9
        5 0 1
        4 3 2
    """
    start = (0, 0)
    left = (-1, 0)
    right = (1, 0)
    up = (0, -1)
    down = (0, 1)

    generator = walk_spiral(start, step=1)

    expected = [
        add(start),  # 0
        add(start, right),  # 1
        add(start, right, down),  # 2
        add(start, right, down, left),  # 3
        add(start, right, down, left, left),  # 4
        add(start, right, down, left, left, up),  # 5
        add(start, right, down, left, left, up, up),  # 6
        add(start, right, down, left, left, up, up, right),  # 7
        add(start, right, down, left, left, up, up, right, right),  # 8
        add(start, right, down, left, left, up, up, right, right, right),  # 9
    ]

    actual = [next(generator) for _ in range(len(expected))]

    assert expected == actual


def add(*parts):
    return sum(part[0] for part in parts), sum(part[1] for part in parts)
