#!/usr/bin/env bash

# Description: This command starts the cblegare/minevisor container and injects a service key into it

# Pre-requisites:
# First create a service account
# Give it object admin permissions through IAM
# mkdir /etc/creds  - create a directory to hold the key which won't be auto backed up
# copy the key here
# then on the VM, run this command

# If you want to check your environment variables successfully were injected from the volume, use the cmd below:
#docker run --volume $DATA:/data --volume /etc/creds:/creds -e GOOGLE_APPLICATION_CREDENTIALS=/creds/key.json --entrypoint bash cblegare/minevisor -c 'cat "$GOOGLE_APPLICATION_CREDENTIALS"'


docker run -v $DATA:/data -v /etc/creds:/creds -e GOOGLE_APPLICATION_CREDENTIALS=/creds/key.json cblegare/minevisor /bin/sh -c 'gcloud auth activate-service-account --key-file=/creds/key.json && gcloud config set project minevisor && minevisor -vvvvv --server-path /data --persistence-url "gs://minevisor/worlds/$SERVERNAME" restore'
