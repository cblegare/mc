worlds["Fjordlynn"] = "/home/cblegare/git/gitlab.com/cblegare/mc/srv/data/fjordlynn"

renders["normalrender"] = {
    "world": "My world",
    "title": "Fjordlynn",
    "northdirection": "upper-left"
}

renders["day"] = {
    "world": "Fjordlynn",
    "title": "Daytime",
    "rendermode": smooth_lighting,
    "dimension": "overworld",
    "northdirection": "upper-left"
}

renders["night"] = {
    "world": "Fjordlynn",
    "title": "Nighttime",
    "rendermode": smooth_night,
    "dimension": "overworld",
    "northdirection": "upper-left"
}

renders["nether"] = {
    "world": "Fjordlynn",
    "title": "Nether",
    "rendermode": nether_smooth_lighting,
    "dimension": "nether",
    "northdirection": "upper-left"
}

renders['biomeover'] = {
    'world': 'Fjordlynn',
    'rendermode': [ClearBase(), BiomeOverlay()],
    'title': "Biome Coloring Overlay",
    'overlay': ['day']
}


outputdir = "/home/cblegare/git/gitlab.com/cblegare/mc/srv/map"
