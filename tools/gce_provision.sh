#!/usr/bin/env bash


INSTANCE_NAME=fjordlynn
MACHINE_TYPE="g1-small"
MACHINE_TYPE="n1-standard-2"
ZONE="northamerica-northeast1-c"
SERVICE_ACCOUNT="fjordlynn-archivist@minevisor.iam.gserviceaccount.com"
CONTAINER_ENV="\
EULA=true,\
LEVEL=$INSTANCE_NAME,\
VERSION=1.15.2,\
FORCE_REDOWNLOAD=true,\
TYPE=PAPER,\
RCON_PASSWORD=minecraft,\
JVM_OPTS=\
-Xms4096M \
-Xmx6040M \
-XX:+UseG1GC \
-XX:+UnlockExperimentalVMOptions \
-XX:MaxGCPauseMillis=100 \
-XX:+DisableExplicitGC \
-XX:TargetSurvivorRatio=90 \
-XX:G1NewSizePercent=50 \
-XX:G1MaxNewSizePercent=80 \
-XX:G1MixedGCLiveThresholdPercent=35 \
-XX:+AlwaysPreTouch \
-XX:+ParallelRefProcEnabled \
-Dusing.aikars.flags=mcflags.emc.gs"


 gcloud compute networks create fjordlynn \
     --subnet-mode=auto

 gcloud compute firewall-rules create \
     fjordlynn-allow-icmp \
     --description="Allows ICMP connections from any source to any instance on the network." \
     --direction=INGRESS \
     --priority=65534 \
     --network=fjordlynn \
     --action=ALLOW \
     --rules=icmp \
     --source-ranges=0.0.0.0/0

 gcloud compute firewall-rules create \
     fjordlynn-allow-internal \
     --description="Allows connections from any source in the network IP range to any instance on the network using all protocols." \
     --direction=INGRESS \
     --priority=65534 \
     --network=fjordlynn \
     --action=ALLOW \
     --rules=all \
     --source-ranges=10.128.0.0/9

 gcloud compute firewall-rules create \
     fjordlynn-allow-ssh \
     --description="Allows TCP connections from any source to any instance on the network using port 22." \
     --direction=INGRESS \
     --priority=65534 \
     --network=fjordlynn \
     --action=ALLOW \
     --rules=tcp:22 \
     --source-ranges=0.0.0.0/0


gcloud compute firewall-rules create minecraft-port-fjordlynn --description "Allow access to external players" --allow tcp:25565 --source-tags=fjordlynn --network=fjordlynn --source-ranges=0.0.0.0/0

gcloud beta compute instances \
    create-with-container $INSTANCE_NAME \
    --description="Minecraft on GCE" \
    --zone="$ZONE" \
    --machine-type="$MACHINE_TYPE" \
    --address $INSTANCE_NAME-external \
    --subnet=projects/minevisor/regions/northamerica-northeast1/subnetworks/fjordlynn \
    --network-tier=PREMIUM \
    --tags=$INSTANCE_NAME \
    --metadata=google-logging-enabled=true \
    --no-restart-on-failure \
    --maintenance-policy=MIGRATE \
    --service-account="$SERVICE_ACCOUNT" \
    --scopes=https://www.googleapis.com/auth/cloud-platform \
    --image=cos-stable-74-11895-125-0 \
    --image-project=cos-cloud \
    --boot-disk-size=20GB \
    --boot-disk-type=pd-ssd \
    --boot-disk-device-name=fjordlynn \
    --metadata-from-file startup-script=gce_startup \
    --container-image=itzg/minecraft-server \
    --container-restart-policy=always \
    --container-arg=--noconsole \
    --container-env="$CONTAINER_ENV" \
    --container-mount-host-path=mount-path=/data,host-path=/tmp/srv/$INSTANCE_NAME,mode=rw \
    --labels=container-vm=cos-stable-74-11895-125-0

# gcloud beta compute --project=fjordlynn instance-templates \
#     create-with-container fjordlynn-template \
#     --machine-type=g1-small \
#     --subnet=projects/fjordlynn/regions/northamerica-northeast1/subnetworks/fjordlynn \
#     --network-tier=PREMIUM \
#     --metadata=google-logging-enabled=true \
#     --metadata-from-file startup-script=tools/gce_startup \
#     --can-ip-forward \
#     --no-restart-on-failure \
#     --maintenance-policy=TERMINATE \
#     --preemptible \
#     --service-account=archivist@fjordlynn.iam.gserviceaccount.com \
#     --scopes=https://www.googleapis.com/auth/cloud-platform \
#     --region=northamerica-northeast1 \
#     --tags=fjordlynn,http-server \
#     --boot-disk-size=10GB \
#     --boot-disk-type=pd-ssd \
#     --boot-disk-device-name=fjordlynn-template \
#     --container-image=itzg/minecraft-server \
#     --container-restart-policy=always \
#     --container-arg=--noconsole \
#     --container-env="$CONTAINER_ENV" \
#     --container-mount-host-path=mount-path=/data,host-path=/tmp/srv/$INSTANCE_NAME,mode=rw \
#     --image=cos-stable-74-11895-125-0 \
#     --image-project=cos-cloud \
#     --labels=container-vm=cos-stable-74-11895-125-0

# gcloud compute --project=fjordlynn instance-groups managed \
#     create fjordlynn \
#     --base-instance-name=fjordlynn \
#     --template=fjordlynn-template \
#     --size=1 \
#     --zone=northamerica-northeast1-a
