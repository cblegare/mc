#!/usr/bin/env bash

#run once

gcloud compute addresses create $INSTANCE_NAME-external \
 --region northamerica-northeast1 \
 --ip-version IPV4
