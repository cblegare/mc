#!/usr/bin/env bash

SERVER_NAME="foo"
VERSION=1.15.2
JVM_OPTS="-XX:+UseG1GC -XX:+UnlockExperimentalVMOptions -XX:MaxGCPauseMillis=100 -XX:+DisableExplicitGC -XX:TargetSurvivorRatio=90 -XX:G1NewSizePercent=50 -XX:G1MaxNewSizePercent=80 -XX:G1MixedGCLiveThresholdPercent=35 -XX:+AlwaysPreTouch -XX:+ParallelRefProcEnabled -Dusing.aikars.flags=mcflags.emc.gs"

docker run \
    -p 25565:25565 \
    -p 8123:8123 \
    -v $(pwd)/srv/data/latest:/data \
    -v $(pwd)/srv/plugins:/plugins \
    --name mc \
    -e EULA=true \
    -e VERSION="$VERSION" \
    -e JVM_OPTS="$JVM_OPTS" \
    -e INIT_MEMORY="1024M" \
    -e MAX_MEMORY="4096M" \
    -e LEVEL="fjordlynn" \
    -e TYPE=PAPER \
    -e RCON_PASSWORD="minecraft" \
    -e OPS=cblegare, Octorax \
    -e SERVER_NAME="$SERVER_NAME" \
    itzg/minecraft-server --noconsole

docker rm mc


