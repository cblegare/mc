import logging
import click
from time import sleep

from fjordlynn import __version__, __package__
from fjordlynn.rcon import RemoteRcon, RconCLI
from fjordlynn.mc import PersistentWorld
from fjordlynn.spiral import walk_spiral


class App(object):
    def __init__(self, rcon: RemoteRcon, world: PersistentWorld):
        self.rcon = rcon
        self.world = world


log = logging.getLogger(__name__)


@click.group()
@click.version_option(version=__version__)
@click.pass_context
@click.option(
    "-v",
    "--verbose",
    "verbosity",
    help="Increase verbosity (can be used multiple times).",
    count=True,
    type=int,
)
@click.option(
    "--server-path",
    help="Local path where is the world (default: /data).",
    default="/data",
)
@click.option(
    "--persistence-url",
    help="URL where backups are managed (a local path works).",
    default="gs://fjordlynn/worlds",
)
@click.option(
    "--rcon-host",
    help="RCON host (default: localhost).",
    default="localhost",
    envvar="RCON_HOST",
)
@click.option(
    "--rcon-port",
    help="RCON port (default: 25575).",
    default=25575,
    envvar="RCON_PORT",
)
@click.option(
    "--rcon-password",
    help="RCON password (default is empty).",
    default="",
    envvar="RCON_PASSWORD",
)
@click.option(
    "--rcon-cli",
    help="If set, RCON is a subprocess (local docker only).",
    default=False,
    is_flag=True,
)
def main(
    ctx,
    verbosity,
    server_path,
    persistence_url,
    rcon_host,
    rcon_port,
    rcon_password,
    rcon_cli,
):
    """fjordlynn cli!"""
    loglevel = verbosity_to_level(verbosity)
    log.error(f"{loglevel!s}")
    logging.basicConfig(level=verbosity_to_level(verbosity))

    if rcon_cli:
        log.info("Setting up CLI rcon")
        rcon = RconCLI()
    else:
        log.info(f"Setting up RCON at {rcon_host!s}:{rcon_port!s}")
        rcon = RemoteRcon(str(rcon_host), str(rcon_password), int(rcon_port))

    world = PersistentWorld(str(server_path), str(persistence_url), rcon)
    log.info(f"World at {server_path!s}")
    log.info(f"Persistence at {persistence_url!s}")
    ctx.obj = App(rcon, world)


@main.command("backup")
@click.pass_obj
@click.option(
    "--wait",
    help="Wait time between backups (default is 15 minutes).",
    default=15 * 60,
)
@click.option(
    "--once", help="Do backup only once and exit.", default=False, is_flag=True
)
def _backup(obj: App, wait, once):
    """Continuously backup world."""
    while True:
        try:
            log.info("Initiating backup")
            obj.world.backup()
            log.info("Backup done")
        except Exception as e:
            log.error(e.args)
        if once:
            log.info("Backup done once as requested. Done!")
            break
        log.info(f"Waiting {wait!s} seconds")
        sleep(wait)


@main.command("restore")
@click.pass_obj
def _restore(obj: App):
    """Restore world from persistence."""
    log.info("Initiating restore")
    obj.world.restore()
    log.info("Restore done")


@main.command("explore")
@click.pass_obj
@click.option(
    "-p", "--player", help="A logged in minecraft player name.", required=True
)
@click.option(
    "-s",
    "--start",
    help="Starting point x:z such as 0.5:0.5 (default: 0:0).",
    default="0:0",
)
@click.option(
    "-r",
    "--radius",
    help="Width of the world you want generated (default: 1000).",
    default=1000,
)
@click.option("--step", help="Length of jumps (default: 16).", default=16)
@click.option(
    "--wait",
    help="Seconds between teleports of jumps (default: 0.5).",
    default=0.5,
)
@click.option(
    "--skip", help="Number of teleport to skip (default: 0).", default=0
)
def _explore(
    obj: App,
    player: str,
    start: str,
    radius: float,
    step: float,
    wait: float,
    skip: int,
):
    """Travel your minecraft world."""
    start = start.split(":", 2)
    start = float(start[0]), float(start[1])
    wait = float(wait)
    skip = int(skip)

    log.info("Exploring world")
    log.info(f"  Puppeteering {player!s}")
    log.info(f"  Starting from {start!s}")
    log.info(f"  Until reached {radius!s} meters radius (square, of course)")

    log.info(f"Setting gamemode to creative for player {player!s}")
    obj.rcon(f"gamemode creative {player!s}")

    count = 0

    for position in walk_spiral(start, step):
        count += 1
        if count < skip + 1:
            continue

        log.info(
            f"Teleporting {player!s} "
            f"to {position[0]!s} 255 {position[1]!s}"
        )
        obj.rcon(f"tp {player!s} {position[0]!s} 255 {position[1]!s}")

        print(f"count {count!s}")

        distance = position[0] - start[0], position[1] - start[1]
        if distance[0] > radius or distance[1] > radius:
            log.info(f"Exploration to radius {radius!s} done")
            break
        sleep(wait)


def verbosity_to_level(
    verbosity: int,
    interval: int = 10,
    start_level: int = logging.CRITICAL,
    most_verbose_level: int = 1,
):
    """
    Find a proper log level for a given verbosity.

    Args:
        verbosity: Number of additional verbosity increments
        interval: Impact of one increment
        start_level: Default verbosity level
        most_verbose_level: Maximum verbosity level

    Returns:
        int: Log level matching the verbosity increments.
    """
    level = start_level - verbosity * interval
    return max(level, most_verbose_level)


if "__main__" == __name__:
    main(prog_name=__package__)
