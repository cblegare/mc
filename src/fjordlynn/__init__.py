from pathlib import Path

from pkg_resources import get_distribution, DistributionNotFound


_PACKAGE = "fjordlynn"
__here__ = str(Path(__file__).parent)


try:
    __dist__ = get_distribution(_PACKAGE)  # type: Distribution
    __project__ = __dist__.project_name
    __version__ = __dist__.version
except DistributionNotFound:
    from pkg_resources import Distribution

    __dist__ = Distribution(project_name=_PACKAGE, version="(local)")
    __project__ = __dist__.project_name
    __version__ = __dist__.version
except ImportError:
    __project__ = _PACKAGE
    __version__ = "(local)"
else:
    pass


class Error(Exception):
    pass
