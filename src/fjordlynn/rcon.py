"""
Original work Copyright (C) 2015 Barnaby Gale
Modified work Copyright (C) 2018 Adrian Turjak

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""


import logging
import socket
import ssl
import select
import struct
import time
from subprocess import run, PIPE, CalledProcessError
from typing import TypeVar


class MCRconException(Exception):
    pass


log = logging.getLogger(__name__)


Rcon = TypeVar("Rcon", bound="BaseRcon")


class MCRcon(object):
    def __enter__(self) -> Rcon:
        self.connect()
        return self

    def __exit__(self, type, value, tb):
        self.disconnect()

    def __call__(self, command: str):
        """Run RCON command"""
        with self as connected:
            connected.command(command)

    def connect(self) -> None:
        raise NotImplementedError

    def disconnect(self) -> None:
        raise NotImplementedError

    def command(self, command: str) -> str:
        raise NotImplementedError


class RconCLI(MCRcon):
    def __init__(self, cmd_prefix: list = None):
        self._cmd_prefix = cmd_prefix or ["docker", "exec", "mc", "rcon-cli"]

    def connect(self) -> None:
        pass

    def disconnect(self) -> None:
        pass

    def command(self, command: str) -> str:
        cmd = self._cmd_prefix.copy()
        cmd.append(command)

        errors = []
        for attempt in range(5):
            try:
                result = run(cmd, stdout=PIPE, check=True)
                return result.stdout.decode("utf8")
            except CalledProcessError as error:
                log.error(
                    f"An error occured at attempt {attempt!s}.", exc_info=error
                )
                errors.append(error)
                time.sleep(0.1)

        raise MCRconException(errors)


class RemoteRcon(MCRcon):
    """
    A client for handling Remote Commands (RCON) to a Minecraft server

    The recommend way to run this client is using the python 'with' statement.
    This ensures that the socket is correctly closed when you are done with it
    rather than being left open.

    Example:

        .. code-block:: python

            from mcrcon import MCRcon

            with MCRcon("10.1.1.1", "sekret") as mcr:
                resp = mcr.command("/whitelist add bob")
                print(resp)

        While you can use it without the 'with' statement, you have to connect
        manually, and ideally disconnect:

        .. code-block:: python

            mcr = MCRcon("10.1.1.1", "sekret")
            mcr.connect()
            resp = mcr.command("/whitelist add bob")
            print(resp)
            mcr.disconnect()
    """

    TLS_DISABLED = 0
    TLS_ENABLED = 1
    TLS_NOVERIFY = 2

    def __init__(self, host, password, port=25575, tlsmode=None):
        self.host = host
        self.password = password
        self.port = port
        self.tlsmode = tlsmode or self.TLS_DISABLED
        self.socket = None

    def connect(self):
        if self.socket is None:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

            # Enable TLS
            if self.tlsmode > 0:
                ctx = ssl.create_default_context()

                # Disable hostname and certificate verification
                if self.tlsmode > 1:
                    ctx.check_hostname = False
                    ctx.verify_mode = ssl.CERT_NONE

                self.socket = ctx.wrap_socket(
                    self.socket, server_hostname=self.host
                )

            self.socket.connect((self.host, self.port))
            self._send(3, self.password)

    def disconnect(self):
        if self.socket is not None:
            self.socket.close()
            self.socket = None

    def _read(self, length):
        data = b""
        while len(data) < length:
            data += self.socket.recv(length - len(data))
        return data

    def _send(self, out_type, out_data):
        if self.socket is None:
            raise MCRconException("Must connect before sending data")

        # Send a request packet
        out_payload = (
            struct.pack("<ii", 0, out_type)
            + out_data.encode("utf8")
            + b"\x00\x00"
        )
        out_length = struct.pack("<i", len(out_payload))
        self.socket.send(out_length + out_payload)

        # Read response packets
        in_data = ""
        while True:
            # Read a packet
            (in_length,) = struct.unpack("<i", self._read(4))
            in_payload = self._read(in_length)
            in_id, in_type = struct.unpack("<ii", in_payload[:8])
            in_data_partial, in_padding = in_payload[8:-2], in_payload[-2:]

            # Sanity checks
            if in_padding != b"\x00\x00":
                raise MCRconException("Incorrect padding")
            if in_id == -1:
                raise MCRconException("Login failed")

            # Record the response
            in_data += in_data_partial.decode("utf8")

            # If there's nothing more to receive, return the response
            if len(select.select([self.socket], [], [], 0)[0]) == 0:
                return in_data

    def command(self, command) -> str:
        errors = []
        for attempt in range(5):
            try:
                result = self._send(2, command)
                time.sleep(0.003)  # MC-72390 workaround
                return result
            except CalledProcessError as error:
                log.error(
                    f"An error occured at attempt {attempt!s}.", exc_info=error
                )
                errors.append(error)
                time.sleep(0.1)
        raise MCRconException(errors)
