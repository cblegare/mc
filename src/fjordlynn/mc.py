from contextlib import contextmanager
from subprocess import run

from fjordlynn.rcon import RemoteRcon

from google.cloud import storage


@contextmanager
def frozen_world(rcon_obj):
    rcon_obj("save-off")
    rcon_obj("save-all")
    yield
    rcon_obj("save-on")


class Transactional(object):
    def __init__(self, root_url: str):
        self.root_url = root_url

    def get_checksums(self):
        client = storage.Client()
        bucket = client.get_bucket("fjordlynn")

        blobs = bucket.list_blobs(prefix="worlds/fjordlynn")

        for blob in blobs:
            print(blob.name)


class PersistentWorld(object):
    def __init__(self, server_dir: str, root_url: str, rcon: RemoteRcon):
        self.rcon = rcon
        self.server_dir = str(server_dir).rstrip("/")
        self.root_url = str(root_url).rstrip("/")

    def backup(self):
        with frozen_world(self.rcon):
            self._upload()
        working_copy_url = self._make_working_copy()
        return working_copy_url

    def restore(self):
        run(
            [
                "gsutil",
                "-m",
                "rsync",
                "-dpr",
                f"{self.root_url}/latest",
                self.server_dir,
            ]
        )

    def _upload(self):
        run(
            [
                "gsutil",
                "-m",
                "rsync",
                "-dpr",
                self.server_dir,
                f"{self.root_url}/active",
            ]
        )

    def _make_working_copy(self):
        working_copy_url = f"{self.root_url}/latest"
        run(
            [
                "gsutil",
                "-m",
                "rsync",
                "-dpr",
                f"{self.root_url}/active",
                working_copy_url,
            ]
        )
        return working_copy_url
