from itertools import count, cycle


def walk_spiral(start: tuple, step: float = 16) -> tuple:
    """
    Generate positions to teleport to.

    This is an infinite generator.

    Args:
        start: Starting position
        step: Length of jumps.

    Yields:
        Position to teleport to in a spiral.
    """
    current_movement_x, current_movement_z = start
    movements = spiral_movements()

    while True:
        yield current_movement_x, current_movement_z
        next_x_movement, next_y_movement = next(movements)
        current_movement_x += next_x_movement * step
        current_movement_z += next_y_movement * step


def spiral_movements():
    for distance, direction in zip(
        _spiral_distances(), _clockwise_directions()
    ):
        for _ in range(distance):
            yield direction


def _spiral_distances():
    for steps in count(1):
        for _ in (0, 1):
            yield steps


def _clockwise_directions():
    left = (-1, 0)
    right = (1, 0)
    up = (0, -1)
    down = (0, 1)
    return cycle((right, down, left, up))
