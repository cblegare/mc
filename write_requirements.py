import os
import configparser
from pathlib import Path


def project_root_dir():
    directory = Path(__file__).parent

    while directory:
        if list(directory.glob("setup.cfg")):
            return directory
        directory = directory.parent

    raise Exception("You break something badly")


def setup_config(project_root_dir):
    config = configparser.ConfigParser()
    config.read(str(project_root_dir / "setup.cfg"))
    return config


def get_requirements(project_info):
    dep_sets = {
        name: [dep.strip() + os.linesep for dep in deps.splitlines() if dep]
        for name, deps in project_info["options.extras_require"].items()
    }
    dep_sets[""] = [
        dep.strip() + os.linesep
        for dep in project_info["options"]["install_requires"].splitlines()
        if dep
    ]
    return dep_sets


def main():
    project_root = project_root_dir()
    project_info = setup_config(project_root)

    for name, deps in get_requirements(project_info).items():
        if name:
            filename = f"requirements_{name!s}.txt"
        else:
            filename = "requirements.txt"

        with Path(project_root, filename).open("w") as opened:
            opened.writelines(deps)


if __name__ == "__main__":
    main()
